import { IonAvatar, IonBackButton, IonBadge, IonButton, IonButtons, IonCardSubtitle, IonCheckbox, IonInput, IonCol, IonContent, IonFooter, IonHeader, IonIcon, IonImg, IonItem, IonItemOption, IonItemOptions, IonItemSliding, IonLabel, IonList, IonNote, IonPage, IonRow, IonTitle, IonToolbar } from "@ionic/react";
import { cart, checkmarkSharp, home, trashOutline, heart, addCircle, removeCircle } from "ionicons/icons";
import { useEffect, useRef, useState } from "react";
import { CartStore, removeFromCart } from "../data/CartStore";
import { ProductStore } from "../data/ProductStore";
import { FavouritesStore } from "../data/FavouritesStore";

import styles from "./CartProducts.module.css";

const CartProducts = () => {

    const cartRef = useRef();
    const products = ProductStore.useState(s => s.products);
    const shopCart = CartStore.useState(s => s.product_ids);
    const favourites = FavouritesStore.useState(s => s.product_ids);
    const [ cartProducts, setCartProducts ] = useState([]);
    const [ amountLoaded, setAmountLoaded ] = useState(6);
    const [ checked, setChecked ] = useState(false);
    

    const [ total, setTotal ] = useState(0);

    useEffect(() => {

        const getCartProducts = () => {

            setCartProducts([]);
            setTotal(0);
            setChecked(false);

            shopCart.forEach(product => {

                var favouriteParts = product.split("/");
                var categorySlug = favouriteParts[0];
                var productID = favouriteParts[1];
                
                const tempCategory = products.filter(p => p.slug === categorySlug)[0];
                const tempProduct = tempCategory.products.filter(p => parseInt(p.id) === parseInt(productID))[0];
                const tempQuantity = 1;

                const tempCartProduct = {

                    category: tempCategory,
                    product: tempProduct,
                    quantity: tempQuantity
                };
                
                /* if (checked){
                    setTotal(prevTotal => prevTotal + parseInt(tempProduct.price.replace("£", "")));
                } */
                setCartProducts(prevSearchResults => [ ...prevSearchResults, tempCartProduct ]);
            });
        }

       

        getCartProducts();
    }, [ shopCart ]);

    
    
    const fetchMore = async (e) => {

		//	Increment the amount loaded by 6 for the next iteration
		setAmountLoaded(prevAmount => (prevAmount + 6));
		e.target.complete();
	}

    const removeProductFromCart = async (index) => {

        removeFromCart(index);
    }

    
          

    const calculate = async (e) => {
        console.log(parseInt(e.product.price.replace("£", "") ) * e.quantity)
        if(checked){
            setTotal(prevTotal => prevTotal + (parseInt(e.product.price.replace("£", "") ) * e.quantity));
        }
        else if (total > 0){
            setTotal(prevTotal => prevTotal - (parseInt(e.product.price.replace("£", "") ) * e.quantity));
        }
    }
    
    const increment = async (e) => {
        
        if(checked){
            e.quantity += 1;
            setTotal(prevTotal => prevTotal + (parseInt(e.product.price.replace("£", "") )));
        }
    }

    const decrement = async (e) => {
        if (e.quantity > 1){
            if(checked){
                e.quantity -= 1;
                setTotal(prevTotal => prevTotal - (parseInt(e.product.price.replace("£", "") )));
            }
        }
    }

    const handleChange = (event, e) => {
        if(checked){
            setTotal(total - (parseInt(e.product.price.replace("£", "") * e.quantity)));
            e.quantity = event.target.value;
            calculate(e);
        }
        
    }
    
    return (

        <IonPage id="category-page" className={ styles.categoryPage }>
            <IonHeader>
				<IonToolbar>
                    <IonButtons slot="start">
                        <IonBackButton color="dark" routerLink="/" routerDirection="back">
                        </IonBackButton>
                        
                    </IonButtons>
					<IonTitle>Cart</IonTitle>
                    
                    <IonButtons slot="end">
						<IonButton color="dark" routerLink="/home">
							<IonIcon className="animate__animated" icon={ home } />
						</IonButton>
                        <IonBadge color="danger">
                            { favourites.length }
                        </IonBadge>
						<IonButton color="danger" routerLink="/favourites">
							<IonIcon icon={ heart } />
						</IonButton>
                        <IonBadge color="dark">
                            { shopCart.length }
                        </IonBadge>
						<IonButton color="dark">
							<IonIcon ref={ cartRef } className="animate__animated" icon={ cart } />
						</IonButton>
					</IonButtons>
				</IonToolbar>
			</IonHeader>
			
			<IonContent fullscreen>

                    <IonRow className="ion-text-center ion-margin-top">
                        <IonCol size="12">
                            <IonNote>{ cartProducts && cartProducts.length } { (cartProducts.length > 1 || cartProducts.length === 0) ? " products" : " product" } found</IonNote>
                        </IonCol>
                    </IonRow>

                    <IonList>
                        { cartProducts && cartProducts.map((product, index) => {

                            if ((index <= amountLoaded)) {
                                return (
                                <IonItemSliding className={ styles.cartSlider }>
                                    <IonItem key={ index } lines="none" detail={ false } className={ styles.cartItem }>
                                        <IonCheckbox onIonChange={e => setChecked(e.detail.checked)} 
                                            onClick={() => calculate(product)}> </IonCheckbox>         

                                        <IonAvatar>
                                            <IonImg src={ product.product.image } />
                                        </IonAvatar>
                                        <IonLabel className="ion-padding-start ion-text-wrap">
                                        <p>{ product.category.name }</p>
                                            <h4>{ product.product.name }</h4>
                                        </IonLabel>

                                        <div className={ styles.quantityInput }>
                                            <IonIcon icon={addCircle} onClick={ () => increment(product) }></IonIcon>
                                            <IonInput   value={product.quantity} placeholder={product.quantity} onBlur={ (event) => handleChange(event, product)}  ></IonInput>
                                            <IonIcon icon={removeCircle} onClick={ () => decrement(product) }></IonIcon>
                                            
                                        </div>

                                            
                                        
                                        <div className={ styles.cartActions }>
                                            
                                            <IonBadge color="dark">{ product.product.price }</IonBadge>
                                        </div>
                                    </IonItem>

                                    <IonItemOptions side="end">
                                        <IonItemOption color="danger" style={{ paddingLeft: "1rem", paddingRight: "1rem" }} onClick={ () => removeProductFromCart(index) }>
                                            <IonIcon icon={ trashOutline } />
                                        </IonItemOption>
                                    </IonItemOptions>
                                </IonItemSliding>
                                );
                            }
                        })}
                    </IonList>
            </IonContent>

            <IonFooter className={ styles.cartFooter }>
                <div className={ styles.cartCheckout }>
                    <IonCardSubtitle>£{ total.toFixed(2) }</IonCardSubtitle>

                    <IonButton color="dark">
                        <IonIcon icon={ checkmarkSharp } />&nbsp;Checkout
                    </IonButton>
                </div>
            </IonFooter>
        </IonPage>
    );
}

export default CartProducts;